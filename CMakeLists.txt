cmake_minimum_required(VERSION 3.2)

project(deye)

find_package(OpenCV 3 REQUIRED)
find_package(Eigen3 REQUIRED)

set(SRC main.cpp obj_rec.cpp)
add_executable(${PROJECT_NAME} ${SRC})
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBRARIES}) 

add_executable(tests test.cpp)
target_link_libraries(tests ${OpenCV_LIBRARIES})
