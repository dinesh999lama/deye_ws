1. Class name 
class BinaryImage
class ColorLab

2. Methode name
Shape GetShape() 
bool IsLargestRect()
Lab GetRgb2Lab()

3. Pointer variable
String *pName, *pAddress

4. private attributes of class
int mHand 
int *mpLeg

5. reference variable in method 
Hand rHand

6. varibale in method 
Hand Hand

7. local variables 
string mininster_name
int minister_pos

8. Global constants
const int M_PI 3.15

9. Static variables
int s_my_name
int sHisName
