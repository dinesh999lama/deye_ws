#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <vector>
#include <iostream>
#include <bits/stdc++.h>
#include <eigen3/Eigen/Dense>
#include <chrono>
#include <map>
#include <sys/ioctl.h>
#include <unistd.h>
namespace DEye {

extern cv::Mat ForegroundImage, BackgroundImage;

enum EnumColSpace{ Lab,Luv,HSV };

struct StructRGB {
  double r;
  double g;
  double b;
};

struct StructLab {
  double L;
  double a;
  double b;
};

double DegToRad(float degrees);

double calcDe2000(StructLab CIE_Lab[2]);

class ClassPixInfo {
public:
  int ObjInd;
  ClassPixInfo() {}
  ~ClassPixInfo() {}
  cv::Point Pos;
  StructRGB RGB;
  StructLab Lab;
  bool IsEdge = false;
};

struct StructPixInfo {
  size_t ObjInd = 0;
  bool IsEdge = false;
};

class Object{
public:
  size_t ObjInd;
  Object() {
    ObjInd = 0;
  }
  ~Object() {}
  std::vector<cv::Point> body_pixs, edge_pixs;
};

struct Line {
  double a;
  double b;
  double c;
  bool operator==(const Line& line) const {
      return a == line.a && b == line.a && c == line.c;
  }
};

class Image {
public:
  Image(cv::Mat inputImg);
  ~Image() {}
  int FrameId;
  std::vector<ClassPixInfo> Pixs;
  int cols, rows;
  std::vector<Object> Objs;
  cv::Mat OrigImg, BackGroundSubtImg, SegmentedImg;
  StructPixInfo PixInfo[640][460];
  Line left, right, buttum, upper;
};

bool isEdge(cv::Point p0,cv::Mat &inputImg);

void getSegmentedImg(const cv::Mat &inputimg,cv::Mat &resImg,DEye::Image &img,const int radi=1);

void getBackgroundSubImg(const cv::Mat &inputImg,cv::Mat &resImg,int FrameId,int de,EnumColSpace colSpace);

void getObjRecWithColor(Image &img,cv::Mat &LabImg,cv::Mat &backgroundSubImg0);

void getShapes(Image img,cv::Mat backgroundSubImg0);

cv::Vec2i getLargestRect(std::vector<cv::Point> &Points,cv::Vec2i maxLB);

std::vector<cv::Point> getAllChildPixs(cv::Point p0,int radi,DEye::Image &img);

enum Direc { clkWise, antiClkWise };
enum KeyPntType { concave, convex, normal };
struct KeyPnt {
  cv::Point pnt;
  KeyPntType keyPntType;
};

enum ParToInc {
  incX,
  incY,
  decX,
  decY,
  null
};

struct PntsToShow {
  cv::Point pos;
  cv::Vec3b color;
};

struct T0Vec{
  cv::Point p0 = {-1,-1};
  cv::Point pN = {-1,-1};
  cv::Point pB = {-1,-1};
  double direc;
};

struct Rect {
  cv::Point p0;
  cv::Point p1;
  cv::Point p2;
  cv::Point p3;
  double height = 0;
  double width = 0;
};

Line GetLineEqFrmPnts(cv::Point p, cv::Point q);

cv::Point getRotatatedNormalizedVec(cv::Point vec,cv::Point p0,double theta);

void GetShapes(Image &img,int radi,Direc direc=clkWise);
}


