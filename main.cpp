#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <iostream>
#include "obj_rec.h"
using namespace std;
using namespace cv;
using namespace Eigen;
int main(int lenArgs, char *args[]){
    int camera_ind = 0;
    if (lenArgs > 1){
        camera_ind = atoi(args[1]);
    }
    else{
        cout<<"Please give an argument to the program!";
        return 1;
    }
  Ptr<BackgroundSubtractor> pBackSubMOG2, pBackSubKNN;
  pBackSubMOG2 = createBackgroundSubtractorMOG2();
  pBackSubKNN = createBackgroundSubtractorKNN();
  cv::VideoCapture cap;
  if(!cap.open(camera_ind))
    return 0;
  cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,460);
  cv::Mat Img;
  int FrameId = 0;
  while(true) {
    cap >> Img;
    DEye::Image img(Img);
    img.FrameId = ++FrameId;
      cv::Mat segImg;
      Mat KNN; pBackSubKNN->apply(Img,KNN);
      cvtColor(KNN,KNN,COLOR_GRAY2BGR);
      DEye::getSegmentedImg(KNN,segImg,img);
      Mat LabImg; cvtColor(Img,LabImg,cv::COLOR_BGR2Lab);
      //DEye::GetShapes(img,1);
      DEye::getObjRecWithColor(img,LabImg,Img);
      //cv::imshow("KNN",KNN);
      //cv::imshow("segment",Img);
      //cv::waitKey(1);
  }
  return 1;
}
