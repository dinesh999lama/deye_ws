#include "obj_rec.h"
namespace DEye {

Line GetLineEqFrmPnts(cv::Point p, cv::Point q)
{
    Line resLine;
    resLine.a = q.y - p.y;
    resLine.b = p.x - q.x;
    resLine.c = resLine.a*(p.x) + resLine.b*(p.y);
    return resLine;
}

Image::Image(cv::Mat inputImg) {
  inputImg.copyTo(Image::OrigImg);
  cols = inputImg.cols;
  rows = inputImg.rows;
  left = GetLineEqFrmPnts(cv::Point(0,1),cv::Point(0,inputImg.rows-2));
  right = GetLineEqFrmPnts(cv::Point(inputImg.cols-1,1),cv::Point(inputImg.cols-1,inputImg.rows-2));
  buttum = GetLineEqFrmPnts(cv::Point(1,0),cv::Point(inputImg.cols-2,0));
  upper = GetLineEqFrmPnts(cv::Point(1,inputImg.rows-1),cv::Point(inputImg.cols-2,inputImg.rows-1));
}

void getSegmentedImg(const cv::Mat &inputImg,cv::Mat &resImg,DEye::Image &img,const int radi) {
  size_t objInd = 0;
  auto start = std::chrono::high_resolution_clock::now();
  for(int i=0;i<inputImg.cols;i++) {
      for(int j=0;j<inputImg.rows;j++) {
        auto col = inputImg.at<cv::Vec3b>(j,i)[0];
        if(img.PixInfo[i][j].ObjInd == 0 && col>100)
        {
          img.PixInfo[i][j].ObjInd = objInd++;
          img.Objs.resize(img.Objs.size()+1);
          img.Objs.back().body_pixs.push_back({i,j});
          size_t CurIndInObj = 0;
          // ---------------- SEGMENTATION OF IMAGE------------------------------//
          do
          {
              cv::Point p0(img.Objs.back().body_pixs[CurIndInObj]);
              bool isEdge = false;
              for(int x=p0.x-radi; x<=p0.x+radi && x<inputImg.cols && x>=0; x++)
              {
                  for(int y=p0.y-radi; y<=p0.y+radi && y<inputImg.rows && y>=0; y++)
                  {
                      cv::Point pc(x,y);
                      if(p0!=pc && img.PixInfo[x][y].ObjInd == 0)
                      {
                          auto cie1 = inputImg.at<cv::Vec3b>(p0.y,p0.x)[0];
                          auto cie2 = inputImg.at<cv::Vec3b>(y,x)[0];
                          if(cie1>100 && cie2>100) {
                              img.PixInfo[x][y].ObjInd = objInd;
                              img.Objs.back().body_pixs.push_back(pc);
                          }
                          else if(isEdge==false) {
                            isEdge = true;
                            img.Objs.back().edge_pixs.push_back(p0);
                          }
                      }
                  }
              }
              CurIndInObj++;
          }
          while(CurIndInObj != img.Objs.back().body_pixs.size());
        }
      }
  }
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  auto FPS = round(1000000.0/duration.count());
  std::cout<<"Segmentation FPS="<<FPS<<"\t"<<duration.count()<<std::endl;
  inputImg.copyTo(resImg);
  for(auto v : img.Objs) {
    for(auto v1 : v.body_pixs) {
      resImg.at<cv::Vec3b>(v1.y,v1.x) = {0,55,0};
    }
    for(auto v1 : v.edge_pixs) {
      resImg.at<cv::Vec3b>(v1.y,v1.x) = {0,255,0};
    }
  }
}

cv::Mat ForegroundImage, BackgroundImage;
void getBackgroundSubImg(const cv::Mat &inputImg,cv::Mat &resImg,int FrameId,int de,EnumColSpace colSpace) {
  if(FrameId<5) {
    switch (colSpace) {
    case Lab: cv::cvtColor(inputImg,BackgroundImage,cv::COLOR_BGR2Lab); break;
    case Luv: cv::cvtColor(inputImg,BackgroundImage,cv::COLOR_BGR2Luv); break;
    case HSV: cv::cvtColor(inputImg,BackgroundImage,cv::COLOR_BGR2HSV); break;
    }
  }
  else {
    switch (colSpace) {
    case Lab: cv::cvtColor(inputImg,ForegroundImage,cv::COLOR_BGR2Lab); break;
    case Luv: cv::cvtColor(inputImg,ForegroundImage,cv::COLOR_BGR2Luv); break;
    case HSV: cv::cvtColor(inputImg,ForegroundImage,cv::COLOR_BGR2HSV); break;
    }
    inputImg.copyTo(resImg);
    for(int x=0;x<ForegroundImage.cols;x++) {
      for(int y=0;y<ForegroundImage.rows;y++) {
        auto CIE_Lab0 = ForegroundImage.at<cv::Vec3b>(y,x);
        auto CIE_Lab1 = BackgroundImage.at<cv::Vec3b>(y,x);
        double De2000 = sqrt(pow(CIE_Lab1[0]-CIE_Lab0[0],2)+pow(CIE_Lab1[1]-CIE_Lab0[1],2)+pow(CIE_Lab1[2]-CIE_Lab0[2],2));
        if(De2000>=de) {
          resImg.at<cv::Vec3b>(y,x) = {255,255,255};
        }
        else {
          resImg.at<cv::Vec3b>(y,x) = {0,0,0};
        }
      }
    }
  }
}

struct PntPos {
  bool insideWinEdge = false;
  bool onWinEdge = false;
  bool outsideWin = false;
  bool onButLeft = false;
  bool onButRight = false;
  bool onTopLeft = false;
  bool onTopRight = false;
  Line line = {-1,-1,-1};
};

struct Vector {
  std::vector<cv::Point> pnts;
  PntPos pntPos;
  cv::Point pntOnWinEdge;
  cv::Point p0;
  cv::Point pN;
};

PntPos GetPntPos(Image &img,cv::Point p0) {
  PntPos pntPos;
  if(int(img.left.a*p0.x + img.left.b*p0.y + img.left.c) == 0) {
    pntPos.line = img.left;
    pntPos.onWinEdge = true;
  }
  else if(int(img.right.a*p0.x + img.right.b*p0.y + img.right.c) == 0) {
    pntPos.line = img.right;
    pntPos.onWinEdge = true;
  }
  else if(int(img.buttum.a*p0.x + img.buttum.b*p0.y + img.buttum.c) == 0) {
    pntPos.line = img.buttum;
    pntPos.onWinEdge = true;
  }
  else if(int(img.upper.a*p0.x + img.upper.b*p0.y + img.upper.c) == 0) {
    pntPos.line = img.upper;
    pntPos.onWinEdge = true;
  }

  if(p0 == cv::Point(0,0)) pntPos.onButLeft = true;
  else if(p0 == cv::Point(img.cols-1,0)) pntPos.onButRight = true;
  else if(p0 == cv::Point(0,img.rows-1)) pntPos.onTopLeft = true;
  else if(p0 == cv::Point(img.cols-1,img.rows-1)) pntPos.onTopRight = true;

  if(p0.x>0 && p0.x<img.cols-1 && p0.y>0 && p0.y<img.rows-1) pntPos.insideWinEdge = true;
  else pntPos.outsideWin = true;
  return pntPos;
}

Vector GetRotVector(Image &rImg,cv::Point p0d,cv::Point pNd,double theta,bool normalized = false) {
  Eigen::Vector3d p0(p0d.x,p0d.y,0);
  Eigen::Vector3d pN(pNd.x,pNd.y,0);
  Eigen::Transform<double, 3, Eigen::Affine> t = Eigen::Transform<double, 3, Eigen::Affine>::Identity();
  t.rotate(Eigen::AngleAxisd((theta * M_PI)/180, Eigen::Vector3d::UnitZ()));
  Eigen::Vector3d res;
  if(normalized)  res = (p0 + t*pN).normalized();
  else res = p0 + t*pN;
  auto P = p0d;
  auto Q = cv::Point(int(round(res.x())),int(round(res.y())));

  Vector vec;
  double radi = norm(Q-P);
  auto normVec = Q-P;
  vec.p0 = P;
  vec.pN = P + radi*normVec;
  for(int r=1;r<=radi-1;r++) {
    auto pntI = P+r*normVec;
    auto pntPos = GetPntPos(rImg,P);
    if(pntPos.insideWinEdge || pntPos.onWinEdge) vec.pnts.push_back(pntI);
    if(pntPos.onWinEdge) vec.pntOnWinEdge = pntI;
  }
  return vec;
}

Vector GetVector(Image &img,cv::Point P,cv::Point Q) {
  Vector vec;
  double radi = norm(Q-P);
  auto normVec = Q-P;
  for(int r=1;r<=radi;r++) {
    auto pntI = P+r*normVec;
    auto pntPos = GetPntPos(img,P);
    if(pntPos.insideWinEdge || pntPos.onWinEdge) vec.pnts.push_back(pntI);
    if(pntPos.onWinEdge) vec.pntOnWinEdge = pntI;
  }
  return vec;
}

bool IsPntInsideWindow(Image &img,cv::Point p0) {
    if(p0.x>=0 && p0.x<img.cols && p0.y>=0 && p0.y<img.rows) return true;
    return false;
}

T0Vec GetT0Vec(Image &img,cv::Point curPnt,cv::Point prevPnt={-1,-1},Direc direc=DEye::clkWise) {
  T0Vec resT0Vec;
  if(prevPnt.x == -1) {
    resT0Vec.direc = atan2(prevPnt.y,prevPnt.x);
    resT0Vec.p0 = prevPnt;
  }
  else {
      cv::Point pB(-1,-1), p0(-1,-1), pN(-1,-1);
      for(int x=curPnt.x-1;x<=curPnt.x+1;x++) {
        for(int y=curPnt.y-1;y<=curPnt.y+1;y++) {
          cv::Point pc(x,y);
          auto parPix = img.PixInfo[curPnt.x][curPnt.y];
          auto childPix = img.PixInfo[x][y];
          if(curPnt != pc && x>=0 && x<img.cols && y>=0 && y<img.rows && parPix.ObjInd == childPix.ObjInd) {
            if(childPix.IsEdge) {
              if(p0.x != -1)  p0 = cv::Point(x,y);
              else pN = cv::Point(x,y) - p0;
            }
            else {
              pB = cv::Point(x,y) - p0;
            }
          }
        }
      }
      if(p0.x != -1 && pN.x != -1 && pB.x != -1) {
        Eigen::Vector3d p0d(p0.x,p0.y,0); Eigen::Vector3d pNd(pN.x,pN.y,0); Eigen::Vector3d pBd(pB.x,pB.y,0);
        if(pBd.cross(p0d).z()>0) {
          switch (direc) {
          case clkWise:
            p0 = curPnt;
            break;
          case antiClkWise:
            break;
          }
        }
        cv::Point T0Vec = pN-p0;
        resT0Vec.pB = pB;
        resT0Vec.direc = atan2(T0Vec.y,T0Vec.x);
        if(resT0Vec.direc < 0)
          resT0Vec.direc = 180-resT0Vec.direc;
      }
  }
  return resT0Vec;
}

void ShowZoomedImg(Image &img,cv::Point p0,int radi,std::vector<PntsToShow> pntsToShow) {
  cv::Mat resImg; img.OrigImg.copyTo(resImg);
 cv::Mat Img(radi*2,radi*2, CV_8UC3, cv::Scalar(0,0,0));
 for(auto pntToShow : pntsToShow) {
   resImg.at<cv::Vec3b>(pntToShow.pos.y,pntToShow.pos.x) = pntToShow.color;
 }
 for(int i=p0.x-radi;i<=p0.x+radi;i++) {
   for(int j=p0.x-radi;j<=p0.y+radi;j++) {
     if(IsPntInsideWindow(img,{i,j})) {
       Img.at<cv::Vec3b>(j,i) = resImg.at<cv::Vec3b>(j,i);
     }
   }
 }
 auto height = img.rows/(2*radi);
 auto width = img.cols/(2*radi);
 cv::resize(Img,Img,cv::Size(),width,height);
 cv::imshow("ZommedImg",Img); cv::waitKey(0);
}

Rect GetLargestRect(Image &img,std::vector<cv::Point> edgePixs,size_t objInd) {
    Rect cur_rect, prev_rect;
    for(size_t i=0;i<edgePixs.size();i++) {
      bool crossedEdge = false;
      for(size_t j=0;j<edgePixs.size() && j != i && crossedEdge==false;j++) {
        cur_rect.p0 = edgePixs[i];
        cur_rect.p1 = edgePixs[j];
        if(cur_rect.p0 != cur_rect.p1) {
          auto pV = GetT0Vec(img,cur_rect.p0).pB;
          auto p2Pos  = GetRotVector(img,cur_rect.p0,cur_rect.p1,90).pN;
          auto p2Neg = GetRotVector(img,cur_rect.p0,cur_rect.p1,-90).pN;
          if(norm(p2Pos-pV)<norm(p2Neg-pV)) {
            cur_rect.p2 = p2Pos;
            cur_rect.p3 = GetRotVector(img,cur_rect.p0,cur_rect.p1,-90,true).pN;
          }
          else {
            cur_rect.p2 = p2Neg;
            cur_rect.p3 = GetRotVector(img,cur_rect.p0,cur_rect.p1,-90,true).pN;
          }
          cv::Mat resImg; img.OrigImg.copyTo(resImg);
          for(auto edge_pix : edgePixs) resImg.at<cv::Vec3b>(edge_pix.y,edge_pix.x) = {0,255,0};
          cv::rectangle(resImg,{cur_rect.p0.x-1,cur_rect.p0.y-1},{cur_rect.p0.x+1,cur_rect.p0.y+1},{0,0,255},1,cv::LINE_8,0);
          cv::rectangle(resImg,{cur_rect.p1.x-1,cur_rect.p1.y-1},{cur_rect.p1.x+1,cur_rect.p1.y+1},{0,0,255},1,cv::LINE_8,0);
          cv::rectangle(resImg,{cur_rect.p2.x-1,cur_rect.p2.y-1},{cur_rect.p2.x+1,cur_rect.p2.y+1},{0,0,255},1,cv::LINE_8,0);
          cv::rectangle(resImg,{cur_rect.p3.x-1,cur_rect.p3.y-1},{cur_rect.p3.x+1,cur_rect.p3.y+1},{0,0,255},1,cv::LINE_8,0);
          struct winsize size;
          ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
          cv::namedWindow("resImg");
          cv::imshow("resImg",resImg); cv::waitKey(0);
          int breath = 1;
          while(!crossedEdge && IsPntInsideWindow(img,cur_rect.p2) && IsPntInsideWindow(img,cur_rect.p3)) {
            cur_rect.p2 = cur_rect.p2*breath++;
            cur_rect.p3 = cur_rect.p3*breath++;
            auto vector = GetVector(img,cur_rect.p2,cur_rect.p3).pnts;
            for(size_t k = 0;k<vector.size() && crossedEdge==false;k++) {
              if(img.PixInfo[vector[k].x][vector[k].y].ObjInd == objInd) {
                crossedEdge = true;
              }
            }
            cur_rect.height = norm(cur_rect.p2-cur_rect.p1);
            cur_rect.width = norm(cur_rect.p1-cur_rect.p0);
            if(!crossedEdge && cur_rect.height>prev_rect.height && cur_rect.width>prev_rect.width) {
              prev_rect = cur_rect;
            }
          }
        }
      }
    }
    return cur_rect;
}

void getObjRecWithColor(Image &img,cv::Mat &LabImg,cv::Mat &backgroundSubImg0){
  cv::RNG rng(12345); int ObjInd = 0;
  std::vector<double> L_body_pixs, a_body_pixs, b_body_pixs, x_body_pixs, y_body_pixs;
  StructLab cie_lab[2];
  for(auto v1 : img.Objs) {
      cv::RotatedRect minRect;
     //if(v1.edge_pixs.size()>10) minRect = GetLargestRect(img,v1.edge_pixs,v1.ObjInd);
    if(!v1.edge_pixs.empty()) minRect = cv::minAreaRect(cv::Mat(v1.edge_pixs));
    if(minRect.size.width>30 && minRect.size.height>30) {
      for(cv::Point v : v1.body_pixs) {
        L_body_pixs.push_back(LabImg.at<cv::Vec3b>(v)[0]);
        a_body_pixs.push_back(LabImg.at<cv::Vec3b>(v)[1]-128);
        b_body_pixs.push_back(LabImg.at<cv::Vec3b>(v)[2]-128);
        x_body_pixs.push_back(v.x);
        y_body_pixs.push_back(v.y);
      }
      cie_lab[0].L =  accumulate(L_body_pixs.begin(), L_body_pixs.end(), 0.0)/L_body_pixs.size();
      cie_lab[0].a =  accumulate(a_body_pixs.begin(), a_body_pixs.end(), 0.0)/a_body_pixs.size();
      cie_lab[0].b =  accumulate(b_body_pixs.begin(), b_body_pixs.end(), 0.0)/b_body_pixs.size();
      double x_avg = accumulate(x_body_pixs.begin(), x_body_pixs.end(), 0.0)/x_body_pixs.size();
      double y_avg = accumulate(y_body_pixs.begin(), y_body_pixs.end(), 0.0)/y_body_pixs.size();

     // std::cout<<cie_lab[0].L<<"\t"<<cie_lab[0].a<<"\t"<<cie_lab[0].b<<std::endl;
      cie_lab[1].L = 134.327;
      cie_lab[1].a = -2.28319;
      cie_lab[1].b = -5.54541;
      cv::Point2f rect_points[4];
      minRect.points( rect_points );
      for( int j = 0; j < 4; j++ )
         line( backgroundSubImg0, rect_points[j], rect_points[(j+1)%4], cv::Scalar(0,0,255), 1, 8 );
      DEye::ClassPixInfo pix;
      for(auto v1 : v1.body_pixs) {
        backgroundSubImg0.at<cv::Vec3b>(v1.y,v1.x) = {0,55,0};
      }
      for(auto v1 : v1.edge_pixs) {
        backgroundSubImg0.at<cv::Vec3b>(v1.y,v1.x) = {0,255,0};
      }
      double cd = sqrt(pow(cie_lab[1].L-cie_lab[0].L,2)+pow(cie_lab[1].a-cie_lab[0].a,2)+pow(cie_lab[1].b-cie_lab[0].b,2));
      if(cd<20) {
        std::cout<<"Monkey="<<ObjInd<<std::endl;
        putText(backgroundSubImg0, "MONKEY"+std::to_string(++ObjInd), cv::Point(x_avg,y_avg),
            cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cvScalar(0,0,255), 2, CV_AA);
      }
    }
  }
}

//*************************************************************************//

cv::Point GetNxtPntOnWinEdge(cv::Point curPar,ParToInc parToInc) {
  switch (parToInc) {
    case incX: return cv::Point(curPar.x+1,curPar.y);
    case decX: return cv::Point(curPar.x+1,curPar.y);
    case incY: return cv::Point(curPar.x+1,curPar.y);
    case decY: return cv::Point(curPar.x+1,curPar.y);
    default: break;
  }
  return {-1,-1};
}

double GetPerpDistFromPointToLine(cv::Point p0,Line line)
{
    return fabs((line.a * p0.x + line.b * p0.y + line.c)) / (sqrt(line.a * line.a + line.b * line.b));
}

ParToInc GetParToInc(Image &img,PntPos pntPos,Direc direc) {
  if(pntPos.onWinEdge) {
    if(pntPos.line == img.left) {
      if(direc==clkWise) return incY;
      else return decY;
    }
    else if(pntPos.line == img.right) {
      if(direc == clkWise) return decY;
      else return incY;
    }
    else if(pntPos.line == img.buttum) {
      if(direc == clkWise) return decX;
      else return incX;
    }
    else if(pntPos.line == img.upper){
      if(direc == clkWise) return incX;
      else return decX;
    }
  }
  else {
    if(pntPos.onButLeft) {
      if(direc==clkWise) return incY;
      else return decY;
    }
    else if(pntPos.onButRight) {
      if(direc==clkWise) return incY;
      else return decY;
    }
    else if(pntPos.onButRight) {
      if(direc==clkWise) return incY;
      else return decY;
    }
    else if(pntPos.onButRight) {
      if(direc==clkWise) return incY;
      else return decY;
    }
  }
  return null;
}

KeyPnt GetNxtKeyPnt(KeyPnt curKeyPnt,KeyPnt prevKeyPnt,int radi,Direc direc,Image &img,size_t objInd) {
  KeyPnt nxt_key_pnt;
  auto t0_vec = GetT0Vec(img,curKeyPnt.pnt,prevKeyPnt.pnt,direc);
  double la = 360.0/(2*M_PI*radi);
  double theta_0 = t0_vec.direc;
  double theta_i = theta_0;
  bool b_nxt_key_pnt_found = false;
  do
  {
    theta_i+=la;
    if(theta_i>=0) theta_i = 0;
    auto rot_vec = GetRotVector(img,curKeyPnt.pnt,prevKeyPnt.pnt,theta_i);
    if(rot_vec.pntPos.onWinEdge)
    {
      auto nxt_pnt = rot_vec.pntOnWinEdge;
      auto par_to_inc = GetParToInc(img,rot_vec.pntPos,direc);
      do
      {
        nxt_pnt = GetNxtPntOnWinEdge(curKeyPnt.pnt,par_to_inc);
      }
      while(img.PixInfo[nxt_pnt.x][nxt_pnt.y].ObjInd != objInd);
      nxt_key_pnt.pnt = nxt_pnt;
      b_nxt_key_pnt_found = true;
    }
    else if(rot_vec.pntPos.insideWinEdge)
    {
      for(auto pix : rot_vec.pnts)
      {
        if(img.PixInfo[pix.x][pix.y].ObjInd == objInd)
        {
          nxt_key_pnt.pnt = pix;
          b_nxt_key_pnt_found = true;
        }
      }
    }
  }
  while(!b_nxt_key_pnt_found && fabs(theta_i)!=fabs(theta_0));

  return nxt_key_pnt;
}

size_t GetNoOfEdgeCovered(Image &img,KeyPnt curKeyPnt,KeyPnt nxtKeyPnt,Direc direc) {
  size_t noOfEdgeCovered = 0;
  while(curKeyPnt.pnt != nxtKeyPnt.pnt) {
    noOfEdgeCovered++;
    curKeyPnt.pnt = GetT0Vec(img,curKeyPnt.pnt,nxtKeyPnt.pnt,direc).p0;
  }
  return noOfEdgeCovered;
}

KeyPntType GetKeyPntTyp(KeyPnt curKeyPnt,KeyPnt prevKeyPnt,KeyPnt nxtKeyPnt) {
  KeyPntType resKeyPntTyp;
  auto p0p1 = prevKeyPnt.pnt-curKeyPnt.pnt;
  auto p0p2 = nxtKeyPnt.pnt-curKeyPnt.pnt;
  auto angle = p0p1.dot(p0p1)/(norm(p0p1)*norm(p0p2));
  if(angle<180) resKeyPntTyp = concave;
  else if(angle>180) resKeyPntTyp = convex;
  else resKeyPntTyp = normal;
  return resKeyPntTyp;
}

void GetShapes(Image &img,int radi,Direc direc) {
  std::vector<KeyPnt> key_pnts;
  for(auto obj : img.Objs) {
      auto objInd = obj.ObjInd;
      key_pnts.push_back({obj.edge_pixs.front()});
      KeyPnt prevKeyPnt;
      size_t noOfEdgeCovered = 0;
      while(noOfEdgeCovered!=obj.edge_pixs.size()) {
        KeyPnt curKeyPnt = key_pnts.back();
        auto nxtKeyPnt = GetNxtKeyPnt(curKeyPnt,prevKeyPnt,radi,direc,img,objInd);
        prevKeyPnt = curKeyPnt;
        curKeyPnt.keyPntType = GetKeyPntTyp(curKeyPnt,prevKeyPnt,nxtKeyPnt);
        key_pnts.push_back(nxtKeyPnt);
        noOfEdgeCovered = GetNoOfEdgeCovered(img,curKeyPnt,nxtKeyPnt,direc);
      }
  }
}

}
