# apply auto completition inside vim
	ctrl+x and ctrl+p or ctrl+n 
# select line and delete inside vim
	shift+v(V) and d
# undo change in vim
	u
# To find given text inside vim
	:?"text" and n to find next	
# go to definition and jump back from definition
	ctrl+] and ctrl+T
# to create tag file for ctags
	ctags -R --exclude=.git .  or ctags -R or ctags -R -f
# to set number 
	set nu
# to set cursor on mouse click
	set mouse=a
# to go to function defintion use tags
	tag func_name or ctrl+mouse click

# to go to function declaration 
	ctrl+) 
# auto completion
 	word + ctrl+n
# to run conquegdb
	ConqueGdbSplit	"file_name" 
	break points and run
//	

cut = d
copy = y
paste = p

# To find file inside given directory
	find /path/to/dir -name "filename"
	eg. find -name "obj_rec.cpp"
# To replace line or word inside file directly from bash
        sed -i 's/old_text/new_text/g' file
# To view file contents without opening it
        more file
# To build cmake project
        cmake -DCMAKE_BUILD_TYPE=Debug or Release
        make
# To insert text inside file directly from bash
	echo "text" >> "file"
 	
# Run gdb debugger
        gdb "executable_name"
# put breakpoint in main executable file
        b "line_no"
        b "func_name"
# print the variable value/info in gdb
        p "variable"
# remove breakpoint in main executable file
        clear "line_no" or clear "func_name" or d "line_number"
# put breakpoint in other source file of main file
        break "path_to_file:29" or break "path_to_file:func_name"

# To go next line in gdb
        n
# To go into step
        s
# To continuew while debugging
        c
# To save breakpoints in file
        save breakpoints "file_name"
# To reload breakpoints from file
        source "file_name"
# To print all the arguments of current stack
        info args
# To print all the local variables
        info locals
# To print all the global variables
        info variables
# To remove the break point from current line 
        clear 
# to add breakpoint in current line 
        b
# to add breakpoint in current file where concque gdb is running 
    b line_number or function
to remove breapoing = clear line number of function name 


    
        
        
 


